# Widget Versus

Simple poll widget made with love just for fun!

----------------------------------------------------------------

## Setup

Clone repository:

`git clone --recursive git@gitlab.com:versus-widget/widget-versus.git`


Install dependencies:

`npm i`

----------------------------------------------------------------

## Running the project

Run the message bot:
-   You can read the [README](https://gitlab.com/versus-widget/tools/chat-bot/-/blob/main/README.md) or do the following
-   Copy the content of `./tools/chet-bot/scr/` ([link](https://gitlab.com/versus-widget/tools/chat-bot/-/blob/main/src/bot.js))
-   Once in your meet call open the chat
-   Open browser console and paste the bot code in the browser console

Start api:
 - On root folder run: `npm run start:api`

Start widget:
 - On root folder run: `npm run start:widget`

Now people on your meet can vote with the following commands:
- `/go class`
- `/go functional`

ENJOY!!!